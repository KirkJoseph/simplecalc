// prjSimpleCalc.cpp : A very simple calculator.
//

#include <iostream>
#include <conio.h>
#include <string>

float Add(float num1, float num2)
{

    float tempCalculation;

    tempCalculation = num1 + num2;

    return tempCalculation;

}

float Subtract(float num1, float num2)
{

    float tempCalculation;

    tempCalculation = num1 - num2;

    return tempCalculation;

}

float Multiply(float num1, float num2)
{

    float tempCalculation;

    tempCalculation = num1 * num2;

    return tempCalculation;

}

bool Divide(float num1, float num2, float &answer)
{
    
    if (num2 == 0) // for division by zero
    {
        answer = NULL;

        return false;

    }
    
    else

    {
        float tempCalculation;

        tempCalculation = num1 / num2;

        answer = tempCalculation;
               
    }
}

int main()
{

    using namespace std; // set namespace

    // declare variables
    float firstInput;
    float secondInput;
    float answer;

    char ArithmaticOperation;
    char calcContinue;

    bool programDone;

    programDone = false;

    while (programDone == false) // main program loop
    {

        cout << "Enter the first positive number: ";
        cin >> firstInput;
        
        while (cin.fail() || firstInput < 1) // if input validation fails, prompt user again, clear input buffer and get new input
        {

            cout << "\nPlease enter a valid positive first number: ";
            cin.clear();

            cin.ignore(numeric_limits<streamsize>::max(), '\n');

            cin >> firstInput;

        }

        cout << "\nEnter the second positive number: ";
        cin >> secondInput;

        while (cin.fail() || secondInput < 1) // if input validation fails, prompt user again, clear input buffer and get new input
        {

            cout << "\nPlease enter a valid positive second number: ";
            cin.clear();

            cin.ignore(numeric_limits<streamsize>::max(), '\n');

            cin >> secondInput;

        }

        cout << "\nEnter the operation (+, -, *, /): ";
        cin >> ArithmaticOperation;

        if (ArithmaticOperation == '+') // operator input validation
        {

            answer = Add(firstInput, secondInput);
            cout << "\nAnswer: " << answer;

        }
        else if (ArithmaticOperation == '-')
        {

            answer = Subtract(firstInput, secondInput);
            cout << "\nAnswer: " << answer;

        }
        else if (ArithmaticOperation == '*')
        {

            answer = Multiply(firstInput, secondInput);
            cout << "\nAnswer: " << answer;

        }
        else if (ArithmaticOperation == '/')
        {

            if (!Divide(firstInput, secondInput, answer)) // if our divide function somehow fails, inform the user
            {

                cout << "\nDivision by zero.";

            }
            else
            {

                cout << "\nAnswer: " << answer;

            }

        }
        else // if none of the expected operators were used, inform the user
        {

            cout << "\nInvalid operator.";

        }

        // prompt user to continue, if they opt to quit change programDone to true to get out of repeat loop
        cout << "\nWould you like to try again? (n/N to quit): ";
        cin >> calcContinue;

        if ((calcContinue == 'n') || (calcContinue == 'N'))
        {

            programDone = true;

        }

    }

    return 0;
}